package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
	trait TestTrees {
		val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
		val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
    val t3 = Leaf('a',3)
	}


  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
      assert(weight(t3)===3)
    }
  }


  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
      assert(chars(t3)===List('a'))
    }
  }


  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("times of a tree") {
    assert(times(List('a','b','a'))===List(('a',2),('b',1)))
    assert(times(List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))=== List((' ',1),(',',1),('d',1),('e',1),('h',1),('l',3),('o',2),('r',1),('w',1)))
  }


  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }


  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("until of code "){
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4),Leaf('z',3))
    assert(until(singleton,combine)(leaflist)=== List(Fork(Fork(Fork(Leaf('e',1),Leaf('t',2),List('e','t'),3),Leaf('z',3),List('e','t','z'),6),Leaf('x',4),List('e','t','z','x'),10)))
  }

  test("createCodeTree of code"){
    val chars1 = List('b','a','b','b','a')
    val chars2 = List('b','a','b','d','a','d','b','d','d')
    assert(createCodeTree(chars1)===Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5))
    assert(createCodeTree(chars2)===Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9))
  }

  test("decode of code"){
    val chars1 = List('e','t','t','z','z','z','x','x','x','x')
    val codeTree1 = createCodeTree(chars1)
    val bits:List[Bit]= List(1,0,1,0,0,1)
    assert(decode(codeTree1,bits)===List('x','z','t'))
  }

  test("decodedSecret in code is"){
    assert(decodedSecret=== List('h','u','f','f','m','a','n','e','s','t','c','o','o','l'))
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
      assert(decode(t2, encode(t2)("abd".toList)) === "abd".toList)
      assert(decode(frenchCode, encode(frenchCode)("huf".toList)) === "huf".toList)
    }
  }

  test("decode and encode a very short text using quickEncode and verifying frenchCode"){
    new TestTrees {
      assert(decode(t1, quickEncode(t1)("ab".toList)) === "ab".toList)
      assert(decode(t2, quickEncode(t2)("abd".toList)) === "abd".toList)
      assert(decode(frenchCode, quickEncode(frenchCode)("huf".toList)) === "huf".toList)
    }
  }

}
